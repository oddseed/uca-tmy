from math import cos,sin,pi
import datetime

#SOLAR RADIATION
def getSolarConst(time):
	"""
	Function that asks for a time returns a integer
	"""
	hours = {7 : -75,8 : -60,9 : -45,10 : -30,11 : -15,12 : 0,13 : 15,14 : 30,15 : 45,16 : 60,17 : 75}
	if time in hours:
		return hours[time]
	else:
		return 0

def getNumberofDay(d,m,a):
	return datetime.datetime(a,m,d).timetuple().tm_yday

#!!!theta (UNUSED)
o = 13.72164
#n (dia del anio)
n = getNumberofDay(17,1,2014)
#GSC [W/m^2] (CONSTANT)
GSC = 1367
#Ion [Wh/m^2] (CHECK!)
ion = GSC*(1+0.033*cos(360.*n/365))
#delta
d = 23.45*sin((360.*(284+n)/365.)*pi/180.)



"""
fecha
hora
temp_interior
temp_exterior
temp_alta
temp_baja
rad_solar
baromtr
lluvia
raz_lluvia
hum_exterior
punto_rocio
"""