#!/usr/bin/python
#Debe estar instalado esta libreria (pip install xlrd)
import xlrd
from math import cos,sin,pi
import datetime
import xlwt
import os

os.environ['DJANGO_SETTINGS_MODULE'] = 'weather.settings'
from tmy.models import TMY,Headers,Data

SHOW_OUTPUT = False

files = os.listdir(os.getcwd())
filtered_files = []
for f in files:
    if f.find('.xls') > 0:
        filtered_files.append(f)

#REMUEVE Output.xls de la lista de xls encontrados
if 'output.xls' in filtered_files:
	filtered_files.pop(filtered_files.index('output.xls'))

#SOLAR RADIATION
def getSolarConst(time):
	"""
	Function that asks for a time returns a integer
	"""
	hours = {7 : -75,8 : -60,9 : -45,10 : -30,11 : -15,12 : 0,13 : 15,14 : 30,15 : 45,16 : 60,17 : 75}
	if time in hours:
		return hours[time]
	else:
		return None

def getNumberofDay(dt):
	return dt.timetuple().tm_yday



#latitud
theta = 13.72164
#n (dia del anio)
n = 17
#GSC [W/m^2] (SOLAR CONSTANT)
GSC = 1367
#go (Constante Solar Traducida a la epoca del anio)
go = GSC*(1+0.033*cos(360.*n/365))
#delta
delta = 23.45*sin((360.*(284+n)/365.)*pi/180.)


def get_average(sheet,col,decimals=1):
	te1 = sheet.cell_value(r+1, col)
	te2 = sheet.cell_value(r+2, col)
	te3 = sheet.cell_value(r+3, col)
	return round(sum([te1,te2,te3])/3.,decimals)

book = xlwt.Workbook()
sh = book.add_sheet('TMY')
gr = 0
for fname in filtered_files:
	workbook = xlrd.open_workbook(fname)
	tmy_header = ['Date (MM/DD/YYYY)','Time (HH:MM)','ETR (W/m^2)','ETRN (W/m^2)','GHI (W/m^2)','GHI source','GHI uncert (%)','DNI (W/m^2)','DNI source','DNI uncert (%)','DHI (W/m^2)','DHI source','DHI uncert (%)','GH illum (lx)','GH illum source','Global illum uncert (%)','DN illum (lx)','DN illum source','DN illum uncert (%)','DH illum (lx)','DH illum source','DH illum uncert (%)','Zenith lum (cd/m^2)','Zenith lum source','Zenith lum uncert (%)','TotCld (tenths)','TotCld source','TotCld uncert (code)','OpqCld (tenths)','OpqCld source','OpqCld uncert (code)','Dry-bulb (C)','Dry-bulb source','Dry-bulb uncert (code)','Dew-point (C)','Dew-point source','Dew-point uncert (code)','RHum (%)','RHum source','RHum uncert (code)','Pressure (mbar)','Pressure source','Pressure uncert (code)','Wdir (degrees)','Wdir source','Wdir uncert (code)','Wspd (m/s)','Wspd source','Wspd uncert (code)','Hvis (m)','Hvis source','Hvis uncert (code)','CeilHgt (m)','CeilHgt source','CeilHgt uncert (code)','Pwat (cm)','Pwat source','Pwat uncert (code)','AOD (unitless)','AOD source','AOD uncert (code)','Alb (unitless)','Alb source','Alb uncert (code)','Lprecip depth (mm)','Lprecip quantity (hr)','Lprecip source','Lprecip uncert (code)']
	# Un 'for' que intera dentro de cada uno de los Worksheets del archivo EXCEL
	for x in range(workbook.nsheets):
		sheet = workbook.sheet_by_index(x)
		print '-'*30
		print 'Starting worksheet',sheet.name,'from file',fname
		print 'Rows',sheet.nrows,'Col',sheet.ncols
		if sheet.nrows == 97 and sheet.ncols == 12:
			print "Normal x"
		else:
			print "Day INCOMPLETE!"
		for r in range(sheet.nrows)[1:]:
			TMY_ROW = {} # Vamos a usar este diccionario por fila
			"""
			ESTAS VARIABLES PARECEN SER CONSTANTES
			"""
			TMY_ROW['6'] = 1
			TMY_ROW['7'] = 0
			TMY_ROW['9'] = 1
			TMY_ROW['10'] = 0
			TMY_ROW['12'] = 1
			TMY_ROW['13'] = 0
			TMY_ROW['14'] = 0
			TMY_ROW['15'] = 1
			TMY_ROW['16'] = 0
			TMY_ROW['17'] = 0
			TMY_ROW['18'] = 1
			TMY_ROW['19'] = 0
			TMY_ROW['20'] = 0
			TMY_ROW['21'] = 1
			TMY_ROW['22'] = 0
			TMY_ROW['23'] = 0
			TMY_ROW['24'] = 1
			TMY_ROW['25'] = 0
			TMY_ROW['26'] = 5
			TMY_ROW['27'] = 'E'
			TMY_ROW['28'] = 9
			TMY_ROW['29'] = 5
			TMY_ROW['30'] = 'E'
			TMY_ROW['31'] = 9
			TMY_ROW['33'] = 'A'
			TMY_ROW['34'] = 7
			TMY_ROW['36'] = 'A'
			TMY_ROW['37'] = 7
			TMY_ROW['39'] = 'A'
			TMY_ROW['40'] = 7
			TMY_ROW['42'] = 'A'
			TMY_ROW['43'] = 7
			TMY_ROW['44'] = 120
			TMY_ROW['45'] = 'A'
			TMY_ROW['46'] = 9
			TMY_ROW['47'] = 0
			TMY_ROW['48'] = 'A'
			TMY_ROW['49'] = 7
			TMY_ROW['50'] = 16100
			TMY_ROW['51'] = 'A'
			TMY_ROW['52'] = 7
			TMY_ROW['53'] = 14911
			TMY_ROW['54'] = 'E'
			TMY_ROW['55'] = 9
			TMY_ROW['56'] = 1.1
			TMY_ROW['57'] = 'E'
			TMY_ROW['58'] = 8
			TMY_ROW['59'] = 0.07
			TMY_ROW['60'] = 'F'
			TMY_ROW['61'] = 8
			TMY_ROW['62'] = None
			TMY_ROW['63'] = None
			TMY_ROW['64'] = None
			TMY_ROW['66'] = 1
			TMY_ROW['67'] = 'A'
			TMY_ROW['68'] = 7
			"""
			ESTAS VARIABLES PARECEN SER CONSTANTES
			"""
			try:
				row = sheet.cell_value(r, 0)
				date = datetime.datetime(*xlrd.xldate_as_tuple(row, workbook.datemode))
				n = getNumberofDay(date)
				#Extraccion de la hora
				hour = sheet.cell_value(r, 1)
				h = int(hour[:hour.find(':')])
				m = int(hour[hour.find(':')+1:-1])
				if hour[-1:] == 'p':
					if h != 12:
						h += 12
				elif hour[-1:] == 'a':
					if h != 12:
						h += 0
					else:
						h = 0
				else:
					print "TIME FORMAT ERROR CHECK THIS!!!"
					raise Exception

				if m == 0:
					gr = gr +1
					TMY_ROW['32'] = get_average(sheet,3) # Temperatura Exterior
					TMY_ROW['5'] = get_average(sheet,6) # Radiacion Solar
					TMY_ROW['35'] = get_average(sheet,11) # Punto de Rocio
					TMY_ROW['38'] = int(get_average(sheet,10,0)) # Humedad Exterior
					TMY_ROW['41'] = int(get_average(sheet,7,0)) # Barometro
					TMY_ROW['65'] = int(sheet.cell_value(r+3, 8))
					d70 = getSolarConst(h+1)

					if d70 == None:
						E70 = 0
					else:
						   #=($D$52)*(1+0.033*COS((360*$G$52*PI())/(365*180)))*(COS($C$5*PI()/180)*COS($H$52*PI()/180)*COS(D70*PI()/180)+SIN($C$5*PI()/180)*SIN($H$52*PI()/180))
						E70= GSC*(1+0.033*cos((360.*n*pi)/(365*180.)))*(cos(theta*pi/180.)*cos(delta*pi/180.)*cos(d70*pi/180.)+sin(theta*pi/180.)*sin(delta*pi/180.))
					d70 = getSolarConst(h+2)
					if d70 == None:
						E71 = 0
					else:
						E71= GSC*(1+0.033*cos((360.*GSC*pi)/(365*180.)))*(cos(theta*pi/180.)*cos(delta*pi/180.)*cos(d70*pi/180.)+sin(theta*pi/180.)*sin(delta*pi/180.))
						
					if E70 == 0:
						TMY_ROW['3'] = 0
						TMY_ROW['5'] = 0.0
					else:
						TMY_ROW['3'] = E70+E71*0.5*1
					#print TMY_ROW['32'],v35,TMY_ROW['38'],v41,v65,E70,TMY_ROW['3']
					TMY_H = 0
					if TMY_ROW['3'] != 0:
						TMY_H = round(TMY_ROW['5']/TMY_ROW['3'],3)
					TMY_I = 0.9511-0.1604*TMY_H+4.388*TMY_H**2-16.638*TMY_H**3+12.336*TMY_H**4
					TMY_ROW['11'] = TMY_ROW['5'] * TMY_I
					TMY_K = TMY_ROW['5'] - TMY_ROW['11']
					TMY_L = 0
					if d70 != None:
						TMY_L =cos(theta*pi/180)*cos(delta*pi/180)*cos(d70*pi/180)+sin(theta*pi/180)*sin(delta*pi/180)
					TMY_ROW['8'] = 0
					TMY_ROW['4'] = 0
					if TMY_L != 0:
						TMY_ROW['8'] =TMY_K/TMY_L
						TMY_ROW['4'] =TMY_ROW['3']/TMY_L
					TMY_ROW['1'] = str(date.year)+'-'+str(date.month)+'-'+str(date.day)
					TMY_ROW['2'] = str(h)+':00'
					v = "Data(1,"
					for c in range(69)[1:]:
						v += "TMY_ROW["+str(c)+"],"
					v = v[:-1]
					v += ").save()"
					exec v
					print "done " + v
					#SHOW THE DATA OUTPUT
					if SHOW_OUTPUT:
						for x in range(69)[1:]:
							print TMY_ROW[str(x)],
						print ''
			except:
				print 'EXCEL FORMAT ERROR CHECK >> ' + fname
                gr = gr - 1
                break

book.save('output.xls')
