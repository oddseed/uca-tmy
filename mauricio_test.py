#!/usr/bin/python
#Debe estar instalado esta libreria (pip install xlrd)
import xlrd
from math import cos,sin,pi
import datetime

#SOLAR RADIATION
def getSolarConst(time):
	"""
	Function that asks for a time returns a integer
	"""
	hours = {7 : -75,8 : -60,9 : -45,10 : -30,11 : -15,12 : 0,13 : 15,14 : 30,15 : 45,16 : 60,17 : 75}
	if time in hours:
		return hours[time]
	else:
		return None

def getNumberofDay(dt):
	return dt.timetuple().tm_yday



#latitud
theta = 13.72164
#n (dia del anio)
n = 17
#GSC [W/m^2] (SOLAR CONSTANT)
GSC = 1367
#go (Constante Solar Traducida a la epoca del anio)
go = GSC*(1+0.033*cos(360.*n/365))
#delta
delta = 23.45*sin((360.*(284+n)/365.)*pi/180.)


fname = 'abril.xls'
workbook = xlrd.open_workbook(fname)

def get_average(sheet,col,decimals=1):
	te1 = sheet.cell_value(r+1, col)
	te2 = sheet.cell_value(r+2, col)
	te3 = sheet.cell_value(r+3, col)
	return round(sum([te1,te2,te3])/3.,decimals)


# Un 'for' que intera dentro de cada uno de los Worksheets del archivo EXCEL
for x in range(workbook.nsheets):
	sheet = workbook.sheet_by_index(x)
	print '-'*35
	print 'Starting worksheet',sheet.name,'from file',fname
	print 'Rows',sheet.nrows,'Col',sheet.ncols

	# Validando que esten completos los datos (if numero de filas y de columnas)
	if sheet.nrows == 97 and sheet.ncols == 12:
		print "Normal x"
	else:
		print "Day INCOMPLETE!"

	for r in range(sheet.nrows)[1:]:
		#Extraccion de la fecha
		row = sheet.cell_value(r, 0)
		date = datetime.datetime(*xlrd.xldate_as_tuple(row, workbook.datemode))
		n = getNumberofDay(date)
		#print 'este es el n',n
		#Extraccion de la hora
		hour = sheet.cell_value(r, 1)
		h = int(hour[:hour.find(':')])
		m = int(hour[hour.find(':')+1:-1])
		if hour[-1:] == 'p':
			if h != 12:
				h += 12
		elif hour[-1:] == 'a':
			if h != 12:
				h += 0
			else:
				h = 0
		else:
			print "TIME FORMAT ERROR CHECK THIS!!!"
			raise Exception

		if m == 0:
			v32 = get_average(sheet,3) # Temperatura Exterior
			v35 = get_average(sheet,11) # Punto de Rocio
			v38 = int(get_average(sheet,10,0)) # Humedad Exterior
			v41 = int(get_average(sheet,7,0)) # Barometro
			v65 = int(sheet.cell_value(r+3, 8))
			d70 = getSolarConst(h+1)
			if d70 == None:
				E70 = 0
			else:
				   #=($D$52)*(1+0.033*COS((360*$G$52*PI())/(365*180)))*(COS($C$5*PI()/180)*COS($H$52*PI()/180)*COS(D70*PI()/180)+SIN($C$5*PI()/180)*SIN($H$52*PI()/180))
				E70= GSC*(1+0.033*cos((360.*n*pi)/(365*180.)))*(cos(theta*pi/180.)*cos(delta*pi/180.)*cos(d70*pi/180.)+sin(theta*pi/180.)*sin(delta*pi/180.))
			d70 = getSolarConst(h+2)
			if d70 == None:
				E71 = 0
			else:
				E71= GSC*(1+0.033*cos((360.*GSC*pi)/(365*180.)))*(cos(theta*pi/180.)*cos(delta*pi/180.)*cos(d70*pi/180.)+sin(theta*pi/180.)*sin(delta*pi/180.))
				
			if E70 == 0:
				F = 0
			else:
				F = E70+E71*0.5*1
			fila = {'2':str(h+1)+':00','32':v32,'35':v35,'38':v38,'41':v41,'65':v65,'70':E70,'F':F}
			print fila['2'],fila['32']
			{0:5,1:10}
			print [1]


		


