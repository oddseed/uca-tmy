#!/usr/bin/python
#Debe estar instalado esta libreria (pip install xlrd)
import xlrd     #excel read
from math import cos,sin,pi,radians,acos,degrees
import datetime
import xlwt                #excel write
import os

os.environ['DJANGO_SETTINGS_MODULE'] = 'weather.settings'
from tmy.models import TMY,Headers,Data

SHOW_OUTPUT = False

files = os.listdir(os.getcwd())
filtered_files = []
for f in files:
    if f.find('.xls') > 0:
        filtered_files.append(f)

#REMUEVE Output.xls de la lista de xls encontrados
if 'output.xls' in filtered_files:
	filtered_files.pop(filtered_files.index('output.xls'))

#HOURLY ANGLE
def hourlyAngle(time):
	"""
	Returns the angle of a given day time (degrees)
	"""
	hours = {7 : -75,8 : -60,9 : -45,10 : -30,11 : -15,12 : 0,13 : 15,14 : 30,15 : 45,16 : 60,17 : 75}
	if time in hours:
		return hours[time]
	else:
		return None

#cos(ZenitAngle)
def cosZenithAngle(rLatitude,rDeclination,rHourlyAngle):
	"""
	Function that returns the cosine of the Zenith angle
	"""
	return cos(rLatitude)*cos(rDeclination)*cos(rHourlyAngle)+sin(rLatitude)*sin(rDeclination)

def getNumberofDay(dt):
	return dt.timetuple().tm_yday

#latitud
latitude = 13.72164
rLatitude = radians(latitude)
#GSC [W/m^2] (SOLAR CONSTANT)
#------------------
GSC = 1367


def get_average(sheet,col,decimals=1):
	te1 = sheet.cell_value(r+1, col)
	te2 = sheet.cell_value(r+2, col)
	te3 = sheet.cell_value(r+3, col)
	return round(sum([te1,te2,te3])/3.,decimals)

book = xlwt.Workbook()
sh = book.add_sheet('TMY')
gr = 0
for fname in filtered_files:
	workbook = xlrd.open_workbook(fname)
	tmy_header = ['Date (MM/DD/YYYY)','Time (HH:MM)','ETR (W/m^2)','ETRN (W/m^2)','GHI (W/m^2)','GHI source','GHI uncert (%)','DNI (W/m^2)','DNI source','DNI uncert (%)','DHI (W/m^2)','DHI source','DHI uncert (%)','GH illum (lx)','GH illum source','Global illum uncert (%)','DN illum (lx)','DN illum source','DN illum uncert (%)','DH illum (lx)','DH illum source','DH illum uncert (%)','Zenith lum (cd/m^2)','Zenith lum source','Zenith lum uncert (%)','TotCld (tenths)','TotCld source','TotCld uncert (code)','OpqCld (tenths)','OpqCld source','OpqCld uncert (code)','Dry-bulb (C)','Dry-bulb source','Dry-bulb uncert (code)','Dew-point (C)','Dew-point source','Dew-point uncert (code)','RHum (%)','RHum source','RHum uncert (code)','Pressure (mbar)','Pressure source','Pressure uncert (code)','Wdir (degrees)','Wdir source','Wdir uncert (code)','Wspd (m/s)','Wspd source','Wspd uncert (code)','Hvis (m)','Hvis source','Hvis uncert (code)','CeilHgt (m)','CeilHgt source','CeilHgt uncert (code)','Pwat (cm)','Pwat source','Pwat uncert (code)','AOD (unitless)','AOD source','AOD uncert (code)','Alb (unitless)','Alb source','Alb uncert (code)','Lprecip depth (mm)','Lprecip quantity (hr)','Lprecip source','Lprecip uncert (code)']
	# Un 'for' que itera para cada uno de los Worksheets del archivo EXCEL
	for x in range(workbook.nsheets):
		sheet = workbook.sheet_by_index(x)
		print '-'*30
		print 'Starting worksheet',sheet.name,'from file',fname
		print 'Rows',sheet.nrows,'Col',sheet.ncols
		if sheet.nrows == 97 and sheet.ncols == 12:
			print "Normal x"
		else:
			print "Day INCOMPLETE!"
		for r in range(sheet.nrows)[1:]:
			TMY_ROW = {} # Vamos a usar este diccionario por fila
			"""
			ESTAS VARIABLES PARECEN SER CONSTANTES
			"""
			TMY_ROW['6'] = 1
			TMY_ROW['7'] = 0
			TMY_ROW['9'] = 1
			TMY_ROW['10'] = 0
			TMY_ROW['12'] = 1
			TMY_ROW['13'] = 0
			TMY_ROW['14'] = 0
			TMY_ROW['15'] = 1
			TMY_ROW['16'] = 0
			TMY_ROW['17'] = 0
			TMY_ROW['18'] = 1
			TMY_ROW['19'] = 0
			TMY_ROW['20'] = 0
			TMY_ROW['21'] = 1
			TMY_ROW['22'] = 0
			TMY_ROW['23'] = 0
			TMY_ROW['24'] = 1
			TMY_ROW['25'] = 0
			TMY_ROW['26'] = 5
			TMY_ROW['27'] = 'E'
			TMY_ROW['28'] = 9
			TMY_ROW['29'] = 5
			TMY_ROW['30'] = 'E'
			TMY_ROW['31'] = 9
			TMY_ROW['33'] = 'A'
			TMY_ROW['34'] = 7
			TMY_ROW['36'] = 'A'
			TMY_ROW['37'] = 7
			TMY_ROW['39'] = 'A'
			TMY_ROW['40'] = 7
			TMY_ROW['42'] = 'A'
			TMY_ROW['43'] = 7
			TMY_ROW['44'] = 120
			TMY_ROW['45'] = 'A'
			TMY_ROW['46'] = 9
			TMY_ROW['47'] = 0
			TMY_ROW['48'] = 'A'
			TMY_ROW['49'] = 7
			TMY_ROW['50'] = 16100
			TMY_ROW['51'] = 'A'
			TMY_ROW['52'] = 7
			TMY_ROW['53'] = 14911
			TMY_ROW['54'] = 'E'
			TMY_ROW['55'] = 9
			TMY_ROW['56'] = 1.1
			TMY_ROW['57'] = 'E'
			TMY_ROW['58'] = 8
			TMY_ROW['59'] = 0.07
			TMY_ROW['60'] = 'F'
			TMY_ROW['61'] = 8
			TMY_ROW['62'] = 999
			TMY_ROW['63'] = 999
			TMY_ROW['64'] = 999
			TMY_ROW['66'] = 1
			TMY_ROW['67'] = 'A'
			TMY_ROW['68'] = 7
			row = sheet.cell_value(r, 0)
			date = datetime.datetime(*xlrd.xldate_as_tuple(row, workbook.datemode))
			#pppppppppppppppppppppppp
			#n (dia del anio)
			n = getNumberofDay(date)
			#Gn (Constante Solar Traducida a la epoca del anio)
			Gn = GSC*(1+0.033*cos(360.*n/365))
			#declination en degrees y radians
			declination = 23.45*sin(2.*pi*(284+n)/365.)
			rDeclination = radians(declination)
			#------------------------
			#Extraccion de la hora
			hour = sheet.cell_value(r, 1)
			h = int(hour[:hour.find(':')])
			m = int(hour[hour.find(':')+1:-1])
			if hour[-1:] == 'p':
				if h != 12:
					h += 12
			elif hour[-1:] == 'a':
				if h != 12:
					h += 0
				else:
					h = 0
			else:
				print "TIME FORMAT ERROR CHECK THIS!!!"
				raise Exception

			if m == 0:
				gr = gr +1
				TMY_ROW['32'] = get_average(sheet,3) # Temperatura Exterior
				TMY_ROW['5'] = get_average(sheet,6) # Radiacion Solar
				TMY_ROW['35'] = get_average(sheet,11) # Punto de Rocio
				TMY_ROW['38'] = int(get_average(sheet,10,0)) # Humedad Exterior
				TMY_ROW['41'] = int(get_average(sheet,7,0)) # Barometro
				TMY_ROW['65'] = int(sheet.cell_value(r+3, 8))#precipitacion pluvia
				 ##pppppppppppp
				dHourlyAngle= hourlyAngle(h)
				if dHourlyAngle == None:
					TMY_ROW['3'] = '0'
					TMY_ROW['4'] = '0'
					TMY_ROW['8'] = '0'
					TMY_ROW['11'] = '0'
				else:
					rHourlyAngle=radians(dHourlyAngle)
					rcosZenithAngle = cosZenithAngle(rLatitude,rDeclination,rHourlyAngle)
					TMY_ROW['4']= str(Gn*rcosZenithAngle)
					rsinZenithAngle=sin(acos(rcosZenithAngle))
					TMY_ROW['3'] = str(Gn*rsinZenithAngle)
					TMY_ROW['8'] = '0'
					TMY_ROW['11'] = '0'
					#-----------------
					#exc={3,5}
				TMY_ROW['1'] = str(date.year)+'-'+str(date.month)+'-'+str(date.day) + ' 0:00'
				TMY_ROW['2'] = str(h)+':00'

				v = "Data(1,1,"
				for c in range(69)[1:]:
					v += "TMY_ROW['"+str(c)+"'],"
				v = v[:-1]
				v += ").save()"
				
				#v="TMY(5,'cccc','Ll',5,12,57,200).save()"      
				#v="TMY(5,'cccc','Ll',5,12,57,200).save()"      #  '1999-12-31 23:59:59',  'AAAA-MM-DD HH24:MI:SS'      
				#TO_TIMESTAMP_TZ('2003/12/13 10:13:18 -8:00', 'YYYY/MM/DD HH:MI:SS TZH:TZM')

				#v= "Data(1,1,TO_TIMESTAMP_TZ('2003/12/13 10:13:18 -8:00', 'YYYY/MM/DD HH:MI:SS TZH:TZM'),TO_TIMESTAMP('1999-12-31 23:59:59',  'AAAA-MM-DD HH24:MI:SS'),4,4,4,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,4,'a',5).save()"
				#print v
				exec v
				#Data(1,1,TO_TIMESTAMP('2003/12/13 10:13:18 -8:00', 'YYYY/MM/DD HH:MI:SS TZH:TZM'),TO_TIMESTAMP('1999-12-31 23:59:59',  'AAAA-MM-DD HH24:MI:SS'),4,4,4,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,'a',5,5,4,'a',5).save()
				print type(TMY_ROW["1"])
				print type(TMY_ROW["2"])
				print type(TMY_ROW["3"])
				print type(TMY_ROW["4"])
				print type(TMY_ROW["5"])
				print type(TMY_ROW["6"])
				print type(TMY_ROW["7"])
				print type(TMY_ROW["8"])
				print type(TMY_ROW["9"])
				print type(TMY_ROW["10"])
				print type(TMY_ROW["11"])
				print type(TMY_ROW["12"])
				print type(TMY_ROW["13"])
				print type(TMY_ROW["14"])
				print type(TMY_ROW["15"])
				print type(TMY_ROW["16"])
				print type(TMY_ROW["17"])
				print type(TMY_ROW["18"])
				print type(TMY_ROW["19"])
				print type(TMY_ROW["20"])
				print type(TMY_ROW["21"])
				print type(TMY_ROW["22"])
				print type(TMY_ROW["23"])
				print type(TMY_ROW["24"])
				print type(TMY_ROW["25"])
				print type(TMY_ROW["26"])
				print type(TMY_ROW["27"])
				print type(TMY_ROW["28"])
				print type(TMY_ROW["29"])
				print type(TMY_ROW["30"])
				print type(TMY_ROW["31"])
				print type(TMY_ROW["32"])
				print type(TMY_ROW["33"])
				print type(TMY_ROW["34"])
				print type(TMY_ROW["35"])
				print type(TMY_ROW["36"])
				print type(TMY_ROW["37"])
				print type(TMY_ROW["38"])
				print type(TMY_ROW["39"])
				print type(TMY_ROW["40"])
				print type(TMY_ROW["41"])
				print type(TMY_ROW["42"])
				print type(TMY_ROW["43"])
				print type(TMY_ROW["44"])
				print type(TMY_ROW["45"])
				print type(TMY_ROW["46"])
				print type(TMY_ROW["47"])
				print type(TMY_ROW["48"])
				print type(TMY_ROW["49"])
				print type(TMY_ROW["50"])
				print type(TMY_ROW["51"])
				print type(TMY_ROW["52"])
				print type(TMY_ROW["53"])
				print type(TMY_ROW["54"])
				print type(TMY_ROW["55"])
				print type(TMY_ROW["56"])
				print type(TMY_ROW["57"])
				print type(TMY_ROW["58"])
				print type(TMY_ROW["59"])
				print type(TMY_ROW["60"])
				print type(TMY_ROW["61"])
				print type(TMY_ROW["62"])
				print type(TMY_ROW["63"])
				print type(TMY_ROW["64"])
				print type(TMY_ROW["65"])
				print type(TMY_ROW["66"])
				print type(TMY_ROW["67"])
				print type(TMY_ROW["68"])

				"""
				tmy_id = models.ForeignKey(TMY) 
				_date = models.DateTimeField()
				_time = models.TimeField()
				_etr = models.IntegerField()
				_etrn = models.IntegerField()
				_ghi = models.IntegerField()
				_ghi_source = models.CharField(max_length=1)
				_ghi_uncert = models.IntegerField()
				_dni = models.IntegerField()
				_dni_source = models.CharField(max_length=1)
				_dni_uncert = models.IntegerField()
				_dhi = models.IntegerField()
				_dhi_source = models.CharField(max_length=1)
				_dhi_uncert = models.IntegerField()
				_gh_illum = models.IntegerField()
				_gh_illum_source = models.CharField(max_length=1)
				_global_illum_uncert = models.IntegerField()
				_dn_illum = models.IntegerField()
				_dn_illum_source = models.CharField(max_length=1)
				_dn_illum_uncert = models.IntegerField()
				_dh_illum = models.IntegerField()
				_dh_illum_source = models.CharField(max_length=1)
				_dh_illum_uncert = models.IntegerField()
				_zenith_lum = models.IntegerField()
				_zenith_lum_source = models.CharField(max_length=1)
				_zenith_lum_uncert = models.IntegerField()
				_totcld = models.IntegerField()
				_totcld_source = models.CharField(max_length=1)
				_totcld_uncert = models.IntegerField()
				_opqcld = models.IntegerField()
				_opqcld_source = models.CharField(max_length=1)
				_opqcld_uncert = models.IntegerField()
				_dry_bulb = models.IntegerField()
				_dry_bulb_source = models.CharField(max_length=1)
				_dry_bulb_uncert = models.IntegerField()
				_dew_point = models.IntegerField()
				_dew_point_source = models.CharField(max_length=1)
				_dew_point_uncert = models.IntegerField()
				_rhum = models.IntegerField()
				_rhum_source = models.CharField(max_length=1)
				_rhum_uncert = models.IntegerField()
				_pressure = models.IntegerField()
				_pressure_source = models.CharField(max_length=1)
				_pressure_uncert = models.IntegerField()
				_wdir = models.IntegerField()
				_wdir_source = models.CharField(max_length=1)
				_wdir_uncert = models.IntegerField()
				_wspd = models.IntegerField()
				_wspd_source = models.CharField(max_length=1)
				_wspd_uncert = models.IntegerField()
				_hvis = models.IntegerField()
				_hvis_source = models.CharField(max_length=1)
				_hvis_uncert = models.IntegerField()
				_ceilhgt = models.IntegerField()
				_ceilhgt_source = models.CharField(max_length=1)
				_ceilhgt_uncert = models.IntegerField()
				_pwat = models.IntegerField()
				_pwat_source = models.CharField(max_length=1)
				_pwat_uncert = models.IntegerField()
				_aod = models.IntegerField()
				_aod_source = models.CharField(max_length=1)
				_aod_uncert = models.IntegerField()
				_alb = models.IntegerField()
				_alb_source = models.CharField(max_length=1)
				_alb_uncert = models.IntegerField()
				_lprecip_depth = models.IntegerField()
				_lprecip_quantity = models.IntegerField()
				_lprecip_source = models.CharField(max_length=1)
				_lprecip_uncert = models.IntegerField()
				"""

"""      
        if SHOW_OUTPUT:
					for x in range(69)[1:]:
						print TMY_ROW[str(x)],
					print ''
		
		except:
		
			print 'EXCEL FORMAT ERROR CHECK >> ' + fname
			gr = gr - 1
			break
"""
#book.save('output.xls')
